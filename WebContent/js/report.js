var temperature = document.getElementById("temperature");
var adult = document.getElementById("adult");
var larva = document.getElementById("larva");
var egg = document.getElementById("egg");
var userName = document.getElementById("userName");
var phone = document.getElementById("phone");
var time = document.getElementById("time");
var detail = document.getElementById("detail");
var op = document.getElementsByTagName("p");
var send = document.getElementById("send");
//正则对象
var regs={
    temperature:/^\d{1,2}$/,
    count:/^\d{1,}$/,
    userName:/^[\u4e00-\u9fa5]{2,5}$/,
    phone:/^[1](3|5|8)\d{9}$/,
    detail:/^[\u4e00-\u9fa5]{2,20}$/
};

//获焦事件，失焦事件，按键抬起事件共同处理一个函数
temperature.onfocus=temperature.onblur=temperature.onkeyup=function(e){
    var _event = window.event||e;//事件对象兼容获取
    checkTemperature(_event);
}
adult.onfocus=adult.onblur=adult.onkeyup=function(){
    var _event = window.event||e;//事件对象兼容获取
    checkAdult(_event);
}
larva.onfocus=larva.onblur=larva.onkeyup=function(e){
    var _event = window.event||e;//事件对象兼容获取
    checkLarva(_event);
}
egg.onfocus=egg.onblur=egg.onkeyup=function(e){
    var _event = window.event||e;//事件对象兼容获取
    checkEgg(_event);
}
userName.onfocus=userName.onblur=userName.onkeyup=function(e){
    var _event = window.event||e;//事件对象兼容获取
    checkUserName(_event);
}
phone.onfocus=phone.onblur=phone.onkeyup=function(e){
    var _event = window.event||e;//事件对象兼容获取
    checkPhone(_event);
}

//检查temperature
function checkTemperature(_e){
    var type;//不同事件类型不同处理方法
    if(_e){
        type = _e.type;
    }
    var val = temperature.value;
    var vlen = val.length;
    console.log(vlen);
    if(type=="focus"){
        console.log("聚焦");
        op[0].className="or";
        op[0].innerHTML = "请输入1~2个数字";
        return false;
    }
    if(type=="blur"){
        console.log("失焦");
        op[0].className = "";
        op[0].innerHTML = "";
        return false;
    }
    if(vlen==0){
        console.log("啥都没有");
        op[0].className = "red";
        op[0].innerHTML = "请输入温度";
        return false;
    }else{
        if(regs.temperature.test(val)){
            
            console.log("输入正确");
            op[0].className = "hide";
            return true;
            
        }else{
            console.log("正则不匹配");
            op[0].className = "red";
            op[0].innerHTML = "格式错误，仅支持1~2位数字";
            return false;
        }
    }
}
//检查adult
function checkAdult(_e){
    var type;//不同事件类型不同处理方法
    if(_e){
        type = _e.type;
    }
    var val = adult.value;
    var vlen = val.length;
    console.log(vlen);
    if(type=="focus"){
        console.log("聚焦");
        op[1].className="or";
        op[1].innerHTML = "请输入一个或多个数字";
        return false;
    }
    if(type=="blur"){
        console.log("失焦");
        op[1].className = "";
        op[1].innerHTML = "";
        return false;
    }
    if(vlen==0){
        console.log("啥都没有");
        op[1].className = "red";
        op[1].innerHTML = "请输入成虫数量";
        return false;
    }else{
        if(regs.count.test(val)){
            
            console.log("输入正确");
            op[1].className = "hide";
            return true;
            
        }else{
            console.log("正则不匹配");
            op[1].className = "red";
            op[1].innerHTML = "格式错误，仅支持一位或多位数字";
            return false;
        }
    }
}
//检查larva
function checkLarva(_e){
    var type;//不同事件类型不同处理方法
    if(_e){
        type = _e.type;
    }
    var val = larva.value;
    var vlen = val.length;
    console.log(vlen);
    if(type=="focus"){
        console.log("聚焦");
        op[2].className="or";
        op[2].innerHTML = "请输入一个或多个数字";
        return false;
    }
    if(type=="blur"){
        console.log("失焦");
        op[2].className = "";
        op[2].innerHTML = "";
        return false;
    }
    if(vlen==0){
        console.log("啥都没有");
        op[2].className = "red";
        op[2].innerHTML = "请输入幼虫数量";
        return false;
    }else{
        if(regs.count.test(val)){
            
            console.log("输入正确");
            op[2].className = "hide";
            return true;
            
        }else{
            console.log("正则不匹配");
            op[2].className = "red";
            op[2].innerHTML = "格式错误，仅支持一位或多位数字";
            return false;
        }
    }
}
//检查虫卵
function checkEgg(_e){
    var type;//不同事件类型不同处理方法
    if(_e){
        type = _e.type;
    }
    var val = egg.value;
    var vlen = val.length;
    console.log(vlen);
    if(type=="focus"){
        console.log("聚焦");
        op[3].className="or";
        op[3].innerHTML = "请输入1~2个数字";
        return false;
    }
    if(type=="blur"){
        console.log("失焦");
        op[3].className = "";
        op[3].innerHTML = "";
        return false;
    }
    if(vlen==0){
        console.log("啥都没有");
        op[3].className = "red";
        op[3].innerHTML = "请输入虫卵数量";
        return false;
    }else{
        if(regs.count.test(val)){
            
            console.log("输入正确");
            op[3].className = "hide";
            return true;
            
        }else{
            console.log("正则不匹配");
            op[3].className = "red";
            op[3].innerHTML = "格式错误，仅支持1~2位数字";
            return false;
        }
    }
}
//检查用户姓名
function checkUserName(_e){
    var type;//不同事件类型不同处理方法
    if(_e){
        type = _e.type;
    }
    var val = userName.value;
    var vlen = val.length;
    console.log(vlen);
    if(type=="focus"){
        console.log("聚焦");
        op[4].className="or";
        op[4].innerHTML = "请输入2~5个汉字";
        return false;
    }
    if(type=="blur"){
        console.log("失焦");
        op[4].className = "";
        op[4].innerHTML = "";
        return false;
    }
    if(vlen==0){
        console.log("啥都没有");
        op[4].className = "red";
        op[4].innerHTML = "请输入用户姓名";
        return false;
    }else{
        if(regs.userName.test(val)){
            
            console.log("输入正确");
            op[4].className = "hide";
            return true;
            
        }else{
            console.log("正则不匹配");
            op[4].className = "red";
            op[4].innerHTML = "格式错误，仅支持2~5位汉字";
            return false;
        }
    }
}
//检查电话
function checkPhone(_e){
    var type;//不同事件类型不同处理方法
    if(_e){
        type = _e.type;
    }
    var val = phone.value;
    var vlen = val.length;
    console.log(vlen);
    if(type=="focus"){
        console.log("聚焦");
        op[5].className="or";
        op[5].innerHTML = "请输入11位数字";
        return false;
    }
    if(type=="blur"){
        console.log("失焦");
        op[5].className = "";
        op[5].innerHTML = "";
        return false;
    }
    if(vlen==0){
        console.log("啥都没有");
        op[5].className = "red";
        op[5].innerHTML = "请输入电话号码";
        return false;
    }else{
        if(regs.phone.test(val)){
            
            console.log("输入正确");
            op[5].className = "hide";
            return true;
            
        }else{
            console.log("正则不匹配");
            op[5].className = "red";
            op[5].innerHTML = "格式错误，仅支持11位数字，首位1，第二位3，5，或8";
            return false;
        }
    }
}



send.onclick = function(){
    if($("#temperature").val()==""||$("#adult").val()==""||$("#larva").val()==""||$("#egg").val()==""||$("#userName").val()==""||$("#phone").val()==""||$("#time").val()==""){
        alert("关键信息位不能为空");
        return false;
    }else{
        if(checkTemperature()&&checkAdult()&&checkLarva()&&checkEgg()&&checkUserName()&&checkPhone()){
            alert("aaa");

            $.ajax({
                type:"get",
                url:"js/report.json",
                dataType:"json",
                data:{
                    temperature:$("#temperature").val(),
                    adult:$("#adult").val(),
                    larva:$("#larva").val(),
                    egg:$("#egg").val(),
                    userName:$("#userName").val(),
                    phone:$("#phone").val(),
                    time:$("#time").val(),
                    detail:$("#detail").val()
                },
                
            success:function(data){
               console.log(data)
            },
            error:function(e){
                console.log(e);
            }
            });
        }
       
        
    }
    }

//fun();



 


