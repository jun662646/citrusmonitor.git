
var dialogInstance,onMoveStartId;//用于记录当前可拖拽的对象

//获取元素
function g(id){
    return document.getElementById(id);
}

//自动居中元素

function autoCenter(el){
    var bodyW = document.documentElement.clientWidth;
    var bodyH = document.documentElement.clientHeight;

    var elW = el.offsetWidth;
    var elH = el.offsetHeight;

    el.style.left = (bodyW-elW)/2 + 'px';
    el.style.top = (bodyH - elH)/2 + 'px';
}

//自动扩展元素到全部显示区域
function fillToBody(el){
    el.style.width = document.documentElement.clientWidth + 'px';
    el.style.height = document.documentElement.clientHeight + 'px';
}

//Dialog实例化的方法
function Dialog(dragId,moveId){
    var instance ={};
    instance.dragElement = g(dragId);//	允许执行 拖拽操作 的元素
    instance.moveElement = g(moveId);//	拖拽操作时，移动的元素

    instance.mouseOffsetLeft = 0;//拖拽操作时，移动元素的起始 X 点

    instance.mouseOffsetTop = 0;//拖拽操作时，移动元素的起始 Y 点

    instance.dragElement.addEventListener('mosedown',function(e){
        var e = e || window.event;

        dialogInstance = instance;
        instance.mouseOffsetLeft = e.pageX - instance.moveElement.offsetLeft;
        instance.mouseOffsetTop = e.pageY - instance.mouseElement.OffsetTop;
    })
    return instance;
}

//在页面中侦听 鼠标弹起事件

document.onmouseup = function(e){
    dialogInstance =false;
    clearInterval(onMoveStartId);
}

//在页面中侦听 鼠标移动事件

document.onmousemove = function(e){
    var e = e || window .event;
    var instance =dialogInstance;
    if(instance){
        var maxX = document.documentElement.clientWidth - instance.moveElement.offsetWidth;
        var maxY = document.documentElement.clientHeight -instance.moveElement.clientHeight;

        instance.moveElement.style.left = Math.min(Math.max((e.pageX - instance.mouseOffsetLeft),0),maxX) + 'px';
        instance.moveElement.style.top = Math.min(Math.max((e.pageY - instance.mouseOffsetTop),0),maxY) + 'px';

    }
    if(e.stopPropagation) {
        e.stopPropagation();
    } else {
        e.cancelBubble = true;
    }
};

//	拖拽对话框实例对象
Dialog('dialogDrag','dialogMove');

function onMoveStart(){

}


		//	重新调整对话框的位置和遮罩，并且展现
		function showDialog(id){
			g(id).style.display = 'block';
			g('mask').style.display = 'block';
			autoCenter( g(id) );
			fillToBody( g('mask') );
		}

		//	关闭对话框
		function hideDialog(id){
			g(id).style.display = 'none';
			g('mask').style.display = 'none';
		}

		//	侦听浏览器窗口大小变化
        //window.onresize = showDialog;
		
		
		var expertLogin = document.getElementById('login');
		
		 expertLogin.onclick = function(){
				$.ajax({
				    type:"post",
				    url:"/citrusmonitor/expert?method=login",
				    dataType:"json",
				    contentType: "application/x-www-form-urlencoded;charset=utf-8",
				    data:{
				    	phone: $('#user-phone').val(),
				    	password:$('#user-password').val()
			    		
				    },
				    	
				    success:function(msg){
				    	
				    	alert("登录成功");
				    },
				    error:function(){
				        alert("登录失败！！");
				    }

				})
		 }
	