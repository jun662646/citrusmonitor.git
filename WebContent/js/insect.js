$.ajax({
    type:"get",
    url:"/citrusmonitor/news?method=show&id_type=1014",
    dataType:"json",
    success:function(data){        
        
        console.log(data);
        var html_tab = "";
        var html_box = "";
        $.each(data,function(i,n){
            html_tab += "<li><svg class=\"li_icon\" aria-hidden=\"true\"><use xlink:href=\"#icon-accessory\"></use></svg>"+n["topic"]+"</li>";
            html_box += "<li><h2>"+n["topic"]+"</h2><img src="+n["img_src"]+">"+n["content"]+"</li>";
        });
        $(".con_tab").append(html_tab);
        $(".con_box").append(html_box);

        tab_fun();
    },
    error:function(){
        alert("资源加载失败！！！");
    }
});


























//选项卡按钮
function tab_fun(){
    var con_tab = document.getElementsByClassName("con_tab")[0];
    var tab_li = con_tab.getElementsByTagName("li");
    var con_box = document.getElementsByClassName("con_box")[0];
    var box_li = con_box.getElementsByTagName("li");
   
    
//初始化样式
    tab_li[0].classList.add("tab_active");
    for(let i=1,len=tab_li.length;i<len;i++){
        box_li[i].classList.add("li_none");
    }


    for(let i=0,len=tab_li.length;i<len;i++){
        tab_li[i].onclick = function(){ 
            for(let j=0;j<len;j++){
                if(!box_li[j].classList.contains("li_none")){
                    box_li[j].classList.add("li_none");
                }
                if(tab_li[j].classList.contains("tab_active")){
                    tab_li[j].classList.remove("tab_active");
                }
                tab_li[i].classList.add("tab_active");
                box_li[i].classList.remove("li_none");
            }
            
        }
    }
}