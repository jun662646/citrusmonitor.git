<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>CET4</title>
</head>
<body>
<div class="list">
			<table class="tablelist">
				<thead>
					<tr>
						<th><input type="checkbox" id="checkbox" onclick="ckALL()"/> 全选</th>
						<th>编号</th>
						<th>姓名</th>
						<th>性别</th>
						<th>专业</th>
						<th>手机号</th>
						<th>自我介绍</th>
						<th>操作</th>
					</tr>
				</thead>
				<c:forEach items="${page.signUp}" var="list" varStatus="vs">
					<tr>
						<td><input type="checkbox" onclick="cancelAll()"></td>
						<td>${list.id }</td>
						<td>${list.name }</td>
						<td>${list.gender }</td>
						<td>${list.classes }</td>
						<td>${list.telephone }</td>
						<td>${list.introduction }</td>			
						<td><a href="#" target="_blank">查看</a> <a href="javascript:warning('${list.id }','${list.name }')">删除</a>
						</td>
					</tr>
				</c:forEach>


			</table>
		</div>
		<div class="page">
			<ul>
				<li>第 <span id="current">${page.currentPage}</span> <span>页/共</span> <span
					id="total">${page.totalPage}</span> <span>页</span>
				</li>
				<li><a href="javascript:first()"> <span id="first">首页</span>
				</a></li>
				<li><a href="javascript:previous()"> <span id="pre">上一页</span>
				</a></li>
				<li><a href="javascript:next()"> <span id="next">下一页</span>
				</a></li>
				<li><a href="javascript:last('${page.totalPage}')"> <span id="last">尾页</span>
				</a></li>
				<li><span> <span>前往第</span> <input id="page" type="text" />
						<span>页</span>
				</span></li>
				<li><a href="javascript:turnPage('${page.totalPage}');" class="find">GO</a></li>
			</ul>
		</div>
	</div>
</body>
</html>