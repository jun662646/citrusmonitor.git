<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isErrorPage="true"%>
<%
	response.setStatus(HttpServletResponse.SC_OK);
%>
<!DOCTYPE html>
<html style="background-color: black;">
<head>
<meta charset="utf-8" />
<title>Error 500 -- Internal Server Error</title>
<style type="text/css">
@media screen and (max-width: 600px) {
	img {width: 100%;}
}
img {position: absolute;top: 0;bottom: 0;left: 0;right: 0;margin: auto;}
</style>
</head>
<body>
	<center>
		<img alt="Error 500 -- Internal Server Error" src="${pageContext.request.contextPath}/error/img/500.jpg"
			ondragstart="return false" oncontextmenu="return false;"
			onselectstart="return false;" />
	</center>
</body>
</html>
