package main.cn.zhku.citrusmonitor.modules.user.service;

import java.util.List;

import main.cn.zhku.citrusmonitor.domain.User;
import main.cn.zhku.citrusmonitor.modules.user.dao.UserDao;
import main.cn.zhku.citrusmonitor.modules.user.dao.impl.UserDaoImpl;

/**
 * @author 成君 943193747@qq.com
 * @Date 2018年6月16日  下午4:27:00
 */
public class UserService {

	private UserDao userDao = new UserDaoImpl();
	
	/**
	 * 添加用户
	 * 
	 * @param user
	 * @return 
	 * @throws Exception	
	 */
	public int add(User user) throws Exception { return userDao.add(user);	}

	/**
	 * 删除用户  一个
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int delete(String id) throws Exception {	return userDao.delete(id);	}

	/**
	 * 修改用户
	 * 
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public int update(User user) throws Exception {	return userDao.update(user);  }

	/**
	 * 根据id获取一个用户
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public User get(String id) throws Exception { return userDao.get(id);  }

	/**
	 * 查询所有		分页展示
	 * 
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public List<User> list(int pageNum, int pageSize,User user) throws Exception {
		
		return userDao.list(pageNum, pageSize, user);  
	}

	/**
	 * 查询总页数
	 * 
	 * @return
	 * @throws Exception
	 */
	public int getCount() throws Exception { return userDao.getCount();	}

	/**
	 * 根据手机号查找用户
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public List<User> login(User user)  throws Exception {
		return userDao.list(1, 10, user);
	}
	
	

	
}
