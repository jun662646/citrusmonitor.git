package main.cn.zhku.citrusmonitor.modules.user.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import main.cn.zhku.citrusmonitor.domain.User;
import main.cn.zhku.citrusmonitor.modules.user.dao.UserDao;
import main.cn.zhku.citrusmonitor.util.DataSourceUtils;
import main.cn.zhku.citrusmonitor.util.StringUtil;

/**
 * UserDao 的实现类
 * 
 * @author 成君 943193747@qq.com
 * @Date 2018年6月16日  下午4:27:56
 */
public class UserDaoImpl implements UserDao {

	@Override
	public int add(User user) throws Exception {
		QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
		String sql = "insert into user(id, username, password,name,gender,phone,email,remark) values(?,?,?,?,?,?,?,?)";
		return qr.update(sql, user.getId(), user.getUsername(), user.getPassword(),user.getName(),user.getGender(),user.getPhone(),user.getEmail(),user.getRemark());
	}

	@Override
	public int delete(String id) throws Exception {
		QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
		String sql = "delete from user where id =? limit 1";
		return qr.update(sql, id);
	}

	@Override
	public int update(User user) throws Exception {
		QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
		String sql = "update user set username=?, password=?, name=?, gender=?, phone=?, email=?, remark=? where id=? limit 1";
		return qr.update(sql, user.getName(), user.getPassword(),user.getName(),user.getGender(),user.getPhone(),user.getEmail(),user.getRemark(),user.getId());
	}

	@Override
	public User get(String id) throws Exception {
		QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
		String sql = "select * from user where id=? limit 1";
		return qr.query(sql, new BeanHandler<>(User.class), id);
	}

	@Override
	public List<User> list(int pageNum, int pageSize,User user) throws Exception {
		
		QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
		String sql = "select * from user where 1=1 ";
		
		ArrayList<Object> params=new ArrayList<>();
		
		//判断参数是否为空 拼接sql
		if (StringUtil.isNotEmpty(user.getUsername())) {
			sql+=(" and username like ? ");
			params.add("%"+user.getUsername()+"%");
		}
		if (StringUtil.isNotEmpty(user.getName())) {
			sql+=(" and name like ? ");
			params.add("%"+user.getName()+"%");
		}
		if (StringUtil.isNotEmpty(user.getPhone())) {
			sql+=(" and phone like ? ");
			params.add("%"+user.getPhone()+"%");
		}
		sql+=(" limit "+(pageNum-1)*pageSize+","+pageSize+"");
		return qr.query(sql, new BeanListHandler<>(User.class), params.toArray());
	}

	/**
	 * 查找总页数
	 */
	@Override
	public int getCount() throws Exception {
		QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
		String sql="select count(*) from user;";
		return ((Long)qr.query(sql, new ScalarHandler())).intValue();
	}

	
	

}
