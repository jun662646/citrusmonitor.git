package main.cn.zhku.citrusmonitor.modules.user.dao;

import java.util.List;

import main.cn.zhku.citrusmonitor.domain.User;

/**
 * @author 成君 943193747@qq.com
 * @Date 2018年6月16日  下午4:27:38
 */
public interface UserDao {

	/**
	 * 添加
	 * 
	 * @param user 各字段
	 * @return user
	 * @throws Exception
	 */
	public int add(User user) throws Exception;
	
	/**
	 * 删除
	 * 
	 * @param id
	 * @return  状态码 1 成功， 0 失败
	 * @throws Exception
	 */
	public int delete(String id) throws Exception;
	
	/**
	 * 修改
	 * 
	 * @param user
	 * @return 状态码 1 成功， 0 失败
	 * @throws Exception
	 */
	public int update(User user) throws Exception;
	
	/**
	 * 根据主键返回 user
	 * 
	 * @param id
	 * @return user
	 * @throws Exception
	 */
	public User get(String id) throws Exception;
	
	/**
	 * 处理多条件查询		分页展示 默认页码：1，每页条数：10
	 * 
	 * @param user
	 * @return user list
	 * @throws Exception
	 */
	public List<User> list(int pageNum, int pageSize,User user) throws Exception;
	
	/**
	 * 查询总条数
	 * 
	 * @return
	 * @throws Exception
	 */
	public int getCount() throws Exception;

}
