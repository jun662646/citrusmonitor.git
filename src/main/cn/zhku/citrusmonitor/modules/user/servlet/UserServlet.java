package main.cn.zhku.citrusmonitor.modules.user.servlet;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import main.cn.zhku.citrusmonitor.domain.User;
import main.cn.zhku.citrusmonitor.modules.user.service.UserService;
import main.cn.zhku.citrusmonitor.util.UUIDUtils;
import main.cn.zhku.citrusmonitor.util.model.Message;
import main.cn.zhku.citrusmonitor.util.model.PageBean;
import main.cn.zhku.citrusmonitor.web.BaseServlet;
import net.sf.json.JSONObject;

/**
 * @author 成君 943193747@qq.com
 * @Date 2018年6月16日 下午4:26:45
 */
public class UserServlet extends BaseServlet {

	private static final long serialVersionUID = 1L;
	private UserService userService = new UserService();

	/**
	 * 添加用户
	 * 
	 * @param request
	 * @param response
	 * @return  json
	 * @throws Exception
	 */
	public JSONObject add(HttpServletRequest request, HttpServletResponse response) throws Exception {
		User user = new User();
		BeanUtils.populate(user, request.getParameterMap());
		user.setId(UUIDUtils.getId());// 设置id
		if (userService.add(user) == 1)
			return JSONObject.fromObject(new Message("1", "添加用户成功"));
		return JSONObject.fromObject(new Message("2", "添加用户失败"));
	}

	/**
	 * 删除用户
	 * 
	 * @param request
	 * @param response
	 * @return  json
	 * @throws Exception
	 */
	public JSONObject delete(HttpServletRequest request, HttpServletResponse response) throws Exception {
		User user = new User();
		BeanUtils.populate(user, request.getParameterMap());
		if (userService.delete(user.getId()) == 1)
			return JSONObject.fromObject(new Message("1", "删除用户成功"));
		return JSONObject.fromObject(new Message("2", "删除用户失败"));
	}
	
	/**
	 * 修改用户信息
	 * 
	 * @param request
	 * @param response
	 * @return  json
	 * @throws Exception
	 */
	public JSONObject edit(HttpServletRequest request, HttpServletResponse response) throws Exception {
		User user = new User();
		BeanUtils.populate(user, request.getParameterMap());
        if(userService.update(user) == 1)
            return JSONObject.fromObject(new Message("1","修改用户成功"));
        else
            return JSONObject.fromObject(new Message("2","修改用户成失败"));
	}
	
	/**
	 * 查看一个用户信息
	 * 
	 * @param request   用户id
	 * @param response
	 * @return  json
	 * @throws Exception
	 */
	public JSONObject show(HttpServletRequest request, HttpServletResponse response) throws Exception {
		User user = new User();
		BeanUtils.populate(user, request.getParameterMap());
		User rUser = userService.get(user.getId());
        if(rUser!=null)
            return JSONObject.fromObject(rUser);
        else
            return JSONObject.fromObject(new Message("2","获取用户信息失败",user.getId()));
	}
	
	/**
	 * 查询所有		分页
	 * @param request
	 * @param response
	 * @return		PageBean  json
	 * @throws Exception
	 */
	public JSONObject list(HttpServletRequest request, HttpServletResponse response) throws Exception {
		User user = new User();
		PageBean<User> page = new PageBean<User>();
		BeanUtils.populate(user, request.getParameterMap());
		BeanUtils.populate(page, request.getParameterMap());
		
		int pageNum = page.getPageNum(); //当前页
		int pageSize = page.getPageSize(); //每页条数
		
		int totalCount = 0; //总条数
		List<User> userList =  userService.list(pageNum, pageSize, user);
		totalCount = userService.getCount();
        //  返回 pageBean
        return JSONObject.fromObject(new PageBean<User>(userList, pageNum, pageSize, totalCount));
	}
	
}
