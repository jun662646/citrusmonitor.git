package main.cn.zhku.citrusmonitor.modules.user.servlet;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import main.cn.zhku.citrusmonitor.domain.User;
import main.cn.zhku.citrusmonitor.modules.user.service.UserService;
import main.cn.zhku.citrusmonitor.util.UUIDUtils;
import main.cn.zhku.citrusmonitor.util.model.Message;
import main.cn.zhku.citrusmonitor.web.BaseServlet;
import net.sf.json.JSONObject;

/**
 * 登录注册
 * 
 * @author 成君 943193747@qq.com
 * @Date 2018年7月8日  下午4:00:30
 */
public class LoginServlet extends BaseServlet{

	private static final long serialVersionUID = 1L;
	private UserService userService = new UserService();
	
	/**
	 * 登录
	 * 
	 * @param request	必须参数:phone，password
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public JSONObject login(HttpServletRequest request, HttpServletResponse response) throws Exception {
		User user = new User();
		user.setPhone(request.getParameter("phone"));
		user.setPassword(request.getParameter("password"));

		List<User> userList = userService.login(user);
		if (userList.size() == 0) 
			return JSONObject.fromObject(new Message("2", "登录失败",user.getPhone()+"不存在"));
		if (userList.get(0) != null && userList.get(0).getPassword().equals(user.getPassword())) {
			request.getSession().invalidate();//干掉session
			request.getSession().setAttribute("user", userList.get(0));//写入session
			return JSONObject.fromObject(new Message("1", "登录成功", user.getUsername()));
		}
		return JSONObject.fromObject(new Message("2", "登录失败", "密码错误"));
	}
	
	/**
	 * 注册
	 * 
	 * @param request	user所有字段
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public JSONObject register(HttpServletRequest request, HttpServletResponse response) throws Exception {
		User user = new User();
		BeanUtils.populate(user, request.getParameterMap());
		user.setId(UUIDUtils.getId());// 设置id
		if (userService.add(user) == 1)
			return JSONObject.fromObject(new Message("1", "用户注册成功",user.getUsername()));
		return JSONObject.fromObject(new Message("2", "用户注册失败",user.getUsername()));
	}
	
	
	/**
	 * 检验手机号码是否已存在
	 * 
	 * @param request	必须参数:phone
	 * @param response
	 * @return		msg：1，可用；2，不可用
	 * @throws Exception
	 */
	public JSONObject CheckPhone(HttpServletRequest request, HttpServletResponse response) throws Exception {
		User user = new User();
		user.setPhone(request.getParameter("phone"));
		List<User> userList = userService.login(user);
		if(userList.size() > 0)
            return JSONObject.fromObject(new Message("2", "手机号码已存在", user.getPhone()));   //  若有相同手机号的用户，返回false
        else
            return JSONObject.fromObject(new Message("1", "手机号码可用", user.getPhone()));    //   否则返回true
	}
	
	/**
	 * 检验用户名是否存在
	 * 
	 * @param request	前端传入的用户名
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public JSONObject checkUsername(HttpServletRequest request, HttpServletResponse response) throws Exception {
		User user = new User();
		user.setUsername(request.getParameter("username"));
		List<User> userList = userService.login(user);
		if(userList.size() > 0)
			return JSONObject.fromObject(new Message("2", "用户名已存在", user.getUsername()));   //  若有相同的用户名，返回false
        else
        	return JSONObject.fromObject(new Message("1", "用户名可用", user.getUsername()));    //   否则返回true
	}
	
	/**
	 * 获取当前用户
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public JSONObject getCurrentUser(HttpServletRequest request, HttpServletResponse response) throws Exception {
		User user = (User) request.getSession().getAttribute("user");
        if (user == null)
        	return JSONObject.fromObject(new Message("2", "当前已登录用户为空",null));
        user.setPassword("xxxxxx");
        //System.out.println(user+"===========================");
        return JSONObject.fromObject(user);
	}
}
