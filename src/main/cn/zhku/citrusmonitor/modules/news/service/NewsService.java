package main.cn.zhku.citrusmonitor.modules.news.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import main.cn.zhku.citrusmonitor.domain.News;
import main.cn.zhku.citrusmonitor.modules.news.dao.NewsDao;
import main.cn.zhku.citrusmonitor.modules.news.dao.NewsDaoImpl;
import main.cn.zhku.citrusmonitor.util.model.PageBean;

public class NewsService {
	NewsDao nd=new NewsDaoImpl();
	
	/**
	 * 获取pageBean对象
	 * @param currentPage
	 * @param pageSize
	 * @return
	 */
	public PageBean<News> findNewsPageBean(int currentPage,int pageSize,News news) {
		int count=0;
		int totalPage;
		List<News> list=null;
		PageBean<News> pbn=new PageBean<>();
		try {
			count=nd.getCount(news);
			
			totalPage=(int)Math.ceil(count*1.0/pageSize);
			list=nd.getNewsBeanList(currentPage,pageSize,news);
			pbn.setList(list);
			pbn.setPageNum(currentPage);
			pbn.setPageSize(pageSize);
			pbn.setTotalCount(count);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return pbn;
		
	}

	public void modifyNews(News news) {
		try {
			nd.modifyNewsByNewsBean(news);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public List<News> showNews(String idType) {
		
		List<News> list=new ArrayList<>();
		try {
			list=nd.getNewsByIdType(idType);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	public void deleteNews(News news) {
		
		try {
			nd.deleteNewsByNewsBean(news);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addNews(News news) {
		// TODO Auto-generated method stub
		try {
			nd.addNewsByNewsBean(news);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public News getNews(String id) {
		// TODO Auto-generated method stub
		try {
			return nd.getNewsById(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	
}
