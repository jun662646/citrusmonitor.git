package main.cn.zhku.citrusmonitor.modules.news.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import main.cn.zhku.citrusmonitor.domain.FerJson;
import main.cn.zhku.citrusmonitor.domain.News;
import main.cn.zhku.citrusmonitor.modules.news.service.NewsService;
import main.cn.zhku.citrusmonitor.util.JsonUtil;
import main.cn.zhku.citrusmonitor.util.model.PageBean;
import main.cn.zhku.citrusmonitor.web.BaseServlet;
import net.sf.json.JSONArray;

public class NewsServlet extends BaseServlet{
	/**
	 * 修改新闻
	 * @param request
	 * @param response
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public void modify(HttpServletRequest request,HttpServletResponse response) throws IllegalAccessException, InvocationTargetException {
		News news=new News();
		NewsService ns=new NewsService();
		BeanUtils.populate(news, request.getParameterMap());
		ns.modifyNews(news);
	}
	/**
	 * 删除某条新闻
	 * @param request
	 * @param reponse
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public void delete(HttpServletRequest request,HttpServletResponse reponse) throws IllegalAccessException, InvocationTargetException {
		News news=new News();
		NewsService ns=new NewsService();
		BeanUtils.populate(news, request.getParameterMap());
		ns.deleteNews(news);
	}
	/**
	 * 
	 * 添加新闻
	 * @param request
	 * @param response
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws IOException 
	 */
	public void add(HttpServletRequest request,HttpServletResponse response) throws IllegalAccessException, InvocationTargetException, IOException {
		News news=new News();
		NewsService ns=new NewsService();
		BeanUtils.populate(news, request.getParameterMap());
		news.setId(UUID.randomUUID().toString());
		Date date=new Date(System.currentTimeMillis());
		//SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//String time=sdf.format(date);
		//System.out.println(sdf.format(date));
		news.setDate(date);
		ns.addNews(news);
		System.out.println(date);
		//response.getWriter().print(news.getDate());
	}
	/**
	 * 获取施肥类型新闻
	 * @param request
	 * @param response
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws IOException 
	 */
	//获取所有该类型新闻
	/*public Object showFer(HttpServletRequest request,HttpServletResponse response) throws IllegalAccessException, InvocationTargetException, IOException {
		News news=new News();
		NewsService ns=new NewsService();	
		String id=request.getParameter("id");
		
		if(id!=null) {              //若需要查询某条新闻，传入id

			return JsonUtil.object2json(ns.getNews(id));
		}
		//System.out.println(request.getServletContext().getRealPath("/news_img/asd.jpg"));
		
		String id_type=request.getParameter("id_type");
		//news=ns.select(id);
		//BeanUtils.populate(news, request.getParameterMap());
		List<News> list=ns.showNews(id_type);  //获取某种新闻内容
//		response.getWriter().print(JSONArray.fromObject(list));
		System.out.println("ab"+list.get(0).getImg_src());
		FerJson fj=new FerJson();
		fj.setLeft_list(JsonUtil.list2json(list));          //封装json
		fj.setRight_list(JsonUtil.list2json(list));
		fj.setRight_title(JsonUtil.list2json(list));
		return JsonUtil.object2json(fj);
	}*/
	/**
	 * 获取所有该类型新闻
	 * @param request
	 * @param response
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws IOException 
	 */
	//获取所有该类型新闻
	public Object show(HttpServletRequest request,HttpServletResponse response) throws IllegalAccessException, InvocationTargetException, IOException {
		News news=new News();
		NewsService ns=new NewsService();	
		String id=request.getParameter("id");
		
		if(id!=null) {              //若需要查询某条新闻，传入id

			return JsonUtil.object2json(ns.getNews(id));
		}
		//System.out.println(request.getServletContext().getRealPath("/news_img/asd.jpg"));
		
		String id_type=request.getParameter("id_type");
		//news=ns.select(id);
		//BeanUtils.populate(news, request.getParameterMap());
		List<News> list=ns.showNews(id_type);  //获取某种新闻内容
//		response.getWriter().print(JSONArray.fromObject(list));
		
		System.out.println("ab"+list.get(0).getImg_src());
		//System.out.println(JsonUtil.list2json(list));
		return JsonUtil.list2json(list);          //封装json
		
	}
	/**
	 *  各条新闻内容分页
	 * @throws IOException 
	 * @throws ServletException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	public Object list(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException, IllegalAccessException, InvocationTargetException{
		NewsService ns=new NewsService();
		
		News news=new News();
		PageBean<News> pbn=new PageBean<News>();	
		BeanUtils.populate(news,request.getParameterMap());  
		BeanUtils.populate(pbn, request.getParameterMap()); //默认一页 十条
			
		pbn=ns.findNewsPageBean(pbn.getPageNum(), pbn.getPageSize(),news);		
		return JsonUtil.object2json(pbn);
	}
}
