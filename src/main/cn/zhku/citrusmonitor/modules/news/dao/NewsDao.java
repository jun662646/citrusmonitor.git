package main.cn.zhku.citrusmonitor.modules.news.dao;

import java.sql.SQLException;
import java.util.List;

import main.cn.zhku.citrusmonitor.domain.News;

public interface NewsDao {

	/**
	 * 获取总数据记录数
	 * @return
	 * @throws SQLException 
	 */
	int getCount(News news) throws SQLException;
	/**
	 * 获取该页news对象
	 * @param currentPage
	 * @param pageSize
	 * @param news 
	 * @return
	 * @throws SQLException 
	 */
	List<News> getNewsBeanList(int currentPage, int pageSize, News news) throws SQLException;
	/**
	 * 修改新闻信息
	 * @param news
	 * @throws SQLException
	 */
	void modifyNewsByNewsBean(News news) throws SQLException;
	/**
	 * 获取新闻
	 * @param id_type
	 * @return
	 * @throws SQLException
	 */
	List<News> getNewsByIdType(String idType) throws SQLException;
	/**
	 * 删除新闻
	 * @param news
	 * @throws SQLException
	 */
	void deleteNewsByNewsBean(News news) throws SQLException;
	/**
	 * 添加新闻
	 * @param news
	 * @throws SQLException
	 */
	void addNewsByNewsBean(News news) throws SQLException;
	/**
	 * 获取某条新闻对象
	 * @param id
	 * @return
	 * @throws SQLException 
	 */
	News selectById(String id) throws SQLException;
	News getNewsById(String id) throws SQLException;
	
	
}
