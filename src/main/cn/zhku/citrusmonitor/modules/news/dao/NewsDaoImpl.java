package main.cn.zhku.citrusmonitor.modules.news.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import main.cn.zhku.citrusmonitor.domain.News;
import main.cn.zhku.citrusmonitor.util.DataSourceUtils;
import main.cn.zhku.citrusmonitor.util.StringUtil;

public class NewsDaoImpl implements NewsDao{
	
	
	/**
	 * 获得记录数
	 */
	@Override
	public int getCount(News news) throws SQLException {
		QueryRunner qr=new QueryRunner(DataSourceUtils.getDataSource());
		String sql="select count(*) from news where 1=1";
		Boolean tag=false;
		ArrayList param=new ArrayList();
		long l;
		
		if(StringUtil.isNotEmpty(news.getId_type())) {
			
			sql+=" and id_type=?";
			param.add(news.getId_type());
			tag=true;
			
		}
		if(StringUtil.isNotEmpty(news.getTopic())) {
			sql+=" and topic like ?";
			param.add("%"+news.getTopic()+"%");
			tag=true;
		}
		if(StringUtil.isNotEmpty(news.getContent())) {
			sql+=" and content like ?";
			param.add("%"+news.getContent()+"%");
			tag=true;
		}
		if(tag) {
			l=(Long)qr.query(sql, new ScalarHandler(1),param.toArray());//返回对象Long
		}else {
			l=(Long)qr.query(sql, new ScalarHandler(1));
		}
		
		return (int)l;
	}

	@Override
	public List<News> getNewsBeanList(int currentPage, int pageSize,News news) throws SQLException{
		QueryRunner qr=new QueryRunner(DataSourceUtils.getDataSource());
		String sql="select * from news where 1=1";
		ArrayList param=new ArrayList();
		
		if(StringUtil.isNotEmpty(news.getId_type())) {
			sql+=" and id_type=?";
			param.add(news.getId_type());
			
		}
		if(StringUtil.isNotEmpty(news.getTopic())) {
			sql+=" and topic like ?";
			param.add("%"+news.getTopic()+"%");
		}
		if(StringUtil.isNotEmpty(news.getContent())) {
			sql+=" and content like ?";
			param.add("%"+news.getContent()+"%");
		}
		sql+=" limit ?,?";
		param.add((currentPage-1)*pageSize);
		param.add(pageSize);
		List<News> list=qr.query(sql, new BeanListHandler<News>(News.class),param.toArray());
		
		return list;
	}

	@Override
	public void modifyNewsByNewsBean(News news) throws SQLException {
		QueryRunner qr=new QueryRunner(DataSourceUtils.getDataSource());
		//News pastNews=this.selectById(news.getId());
		List<Object> param=new ArrayList<>();
		String sql="update news set id=?";
		param.add(news.getId());
		//id_type=?,topic=?, content=?, picture=?,id_publisher=?, remark=? where id=?
		if(StringUtil.isNotEmpty(news.getId_type())) {
			sql+=",id_type=?";
			param.add(news.getId_type());
			
		}
		if(StringUtil.isNotEmpty(news.getTopic())) {
			sql+=",topic=?";
			param.add(news.getTopic());
		}
		if(StringUtil.isNotEmpty(news.getContent())) {
			sql+=",content=?";
			param.add(news.getContent());
		}
		if(StringUtil.isNotEmpty(news.getImg_src())) {
			sql+=",img_src=?";
			param.add(news.getImg_src());
		}
		if(StringUtil.isNotEmpty(news.getId_publisher())) {
			sql+=",id_publisher=?";
			param.add(news.getId_publisher());
		}
		if(StringUtil.isNotEmpty(news.getRemark())) {
			sql+=",remark=?";
			param.add(news.getRemark());
		}
		//String sql="update news set id_type=?,topic=?, content=?, picture=?,id_publisher=?, remark=? where id=?";
		sql+="where id=?";
		param.add(news.getId());
		qr.update(sql,param);
		
	}

	@Override
	public List<News> getNewsByIdType(String idType) throws SQLException {
		QueryRunner qr=new QueryRunner(DataSourceUtils.getDataSource());
		String sql="select * from news where id_type=?";
		//News news=this.selectById(id);
		List<News> list=qr.query(sql,new BeanListHandler<News>(News.class),idType);
		return list;
	}

	@Override
	public void deleteNewsByNewsBean(News news) throws SQLException {
		QueryRunner qr=new QueryRunner(DataSourceUtils.getDataSource());
		String sql="delete from news where id=?";
		
		qr.update(sql,news.getId());
		
	}

	@Override
	public void addNewsByNewsBean(News news) throws SQLException {
		// TODO Auto-generated method stub
		QueryRunner qr=new QueryRunner(DataSourceUtils.getDataSource());
		String sql="insert into news(id,id_type,topic,content,img_src,id_publisher,remark,date) values(?,?,?,?,?,?,?,?)";
		
		qr.update(sql,news.getId(),news.getId_type(),news.getTopic(),news.getContent(),news.getImg_src(),news.getId_publisher(),news.getRemark(),news.getDate());
	}

	@Override
	public News selectById(String id) throws SQLException {
		QueryRunner qr=new QueryRunner(DataSourceUtils.getDataSource());
		String sql="select * from news where id=?";
		
		return qr.query(sql, new BeanHandler<News>(News.class),id);
	}

	@Override
	public News getNewsById(String id) throws SQLException {
		QueryRunner qr=new QueryRunner(DataSourceUtils.getDataSource());
		String sql="select * from news where id=?";
		News news=qr.query(sql,new BeanHandler<News>(News.class),id);
		return news;
	}

	

}
