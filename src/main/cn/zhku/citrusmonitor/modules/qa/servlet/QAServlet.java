package main.cn.zhku.citrusmonitor.modules.qa.servlet;

import java.util.List;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.fileupload.FileUploadBase.FileSizeLimitExceededException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import main.cn.zhku.citrusmonitor.domain.QA;
import main.cn.zhku.citrusmonitor.domain.User;
import main.cn.zhku.citrusmonitor.modules.qa.service.QAService;
import main.cn.zhku.citrusmonitor.util.UUIDUtils;
import main.cn.zhku.citrusmonitor.util.WebUtils;
import main.cn.zhku.citrusmonitor.util.model.Message;
import main.cn.zhku.citrusmonitor.web.BaseServlet;
import net.sf.json.JSONObject;

public class QAServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
	private QAService qaService = new QAService();
	
	
	/*
	public JSONObject addQ(HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		QA qa=new QA();
		
		BeanUtils.populate(qa, request.getParameterMap());			
		User user= (User) request.getSession().getAttribute("user");  //session 中拿到提问者user对象
		if(user!=null) {		
		qa.setPhone(user.getPhone());
		qa.setName(user.getName());		
		}
		qa.setId(UUIDUtils.getId());         //设置opid				
		if(qaService.addQ(qa)==1)
			return JSONObject.fromObject(new Message("1", "问题提交成功"));
		return JSONObject.fromObject(new Message("2", "问题提交失败"));			
		
		

	}
	
	public JSONObject addA(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		QA qa=new QA();
		
		String rcont=request.getParameter("rcont");
		String qid=request.getParameter("qid");
		qa.setReply_content(rcont);
		qa.setId(qid);
		
		User user= (User) request.getSession().getAttribute("user");  
		if(user!=null)
		{
			qa.setId_responser(user.getId());
		}
		
		if(qaService.addA(qa)==1)
			return JSONObject.fromObject(new Message("1", "问题回答成功"));
		return JSONObject.fromObject(new Message("2", "问题回答失败"));			

	}
	*/


	//上传文件
	
	public JSONObject UploadServlet(HttpServletRequest request, HttpServletResponse response) throws Exception {
			
			if (!ServletFileUpload.isMultipartContent(request)) {  
		        return JSONObject.fromObject(new Message("2", "不是请求的信息表单，请确认表单属性是否正确")); 
		    }  
		      
		    try {  
		        //调用工具类，获得上传文件信息  
		    	
		    	//用于判断上传者是问者还是答者
		    	User user=(User) request.getSession().getAttribute("user");
		    	String userid=user.getId();
		    	//根据id查询role
		    	QAService qaService = new QAService();
		    	String role=qaService.getRole(userid);
		   	
		    	    					    			
		        QA qa=WebUtils.doFileUpload(request,role);  
		        QAService service=new QAService();            
		        service.insert(qa,role);//保存上传文件信息  
		          
		        return JSONObject.fromObject(new Message("1", "上传成功"));
		      
		    } catch (FileSizeLimitExceededException e) {  
		        return JSONObject.fromObject(new Message("2", "对不起，您上传的文件大小超过了大小的限制"));
		    } catch (Exception e) {
				e.printStackTrace();
			}
			return null;  
		}

	//查找所有未回答问题
	public JSONObject searchAll(HttpServletRequest request, HttpServletResponse response) throws Exception {
		    List<QA> quesList=qaService.searchAll();
		    
		    if(quesList.isEmpty())
				return JSONObject.fromObject(new Message("2", "没有待答问题"));
		   return JSONObject.fromObject(quesList);
		}
	
	
	//查找热门问题
	public JSONObject searchHot(HttpServletRequest request, HttpServletResponse response) throws Exception {
		    List<QA> hotList=qaService.searchHot();
		    
		    if(hotList.isEmpty())
				return JSONObject.fromObject(new Message("2", "没有热门问题"));
		   return JSONObject.fromObject(hotList);
		}
	
	
	
}
