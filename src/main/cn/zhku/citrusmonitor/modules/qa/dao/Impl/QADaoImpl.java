package main.cn.zhku.citrusmonitor.modules.qa.dao.Impl;


import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import main.cn.zhku.citrusmonitor.domain.QA;
import main.cn.zhku.citrusmonitor.modules.qa.dao.QADao;
import main.cn.zhku.citrusmonitor.util.DataSourceUtils;

/**
 * @author Ade
 *
 *@date 2018年6月18日 下午10:26:49 
 */
public class QADaoImpl implements QADao {
/*
	public int addQ(QA qa) throws Exception {
		
		QueryRunner qr=new QueryRunner(DataSourceUtils.getDataSource());		
		String sql="insert into q_a(id,name,phone,theme,q_details,remark) values(?,?,?,?,?,?)";	
		return qr.update(sql, qa.getId(),qa.getName(),qa.getPhone(),qa.getTheme(),qa.getRemark(),qa.getQ_details());
	}

	public int addA(QA qa) throws Exception {

		QueryRunner qr=new QueryRunner(DataSourceUtils.getDataSource());		
		String sql="update q_a set reply_content=?,id_responser=? where id=?";	
		return qr.update(sql, qa.getReply_content(),qa.getId_responser(),qa.getId());
		
	}
*/
	public List<QA> searchAll() throws Exception {
		QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
		String sql = "select * from q_a where status='0'";
		return qr.query(sql, new BeanListHandler<>(QA.class));
	}


	@Override
	public String getRole(String userid) throws Exception {
		QueryRunner qr=new QueryRunner(DataSourceUtils.getDataSource());		
		String sql="select role from role where id=?";	
		return (String) qr.query(sql, new ScalarHandler(1), userid);
	}
	
	
	
	@Override
	public void insert(QA qa,String role) {// 上传文件  
   	 QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());  
       /* 
        *  
        * private String id; private String uuidname; //上传文件的名称，文件的uuid名 
        * private String filename; //上传文件的真实名称 private String savepath; 
        * //记住文件的位置 private Date uploadtime; //文件的上传时间 private String 
        * description; //文件的描述 private String username; 
        */  
   	 
   	 
   	 SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
   	 
   	 
   	 if(role.equals("que"))
        {
   		 qa.setQ_date(df.format(new Date()));// new Date()为获取当前系统时间
   		 
   		  Object[] param = { qa.getId(),qa.getName(),qa.getPhone(),qa.getTheme(),qa.getQ_details(),qa.getRemark(), 
   	        		qa.getQ_filename(),qa.getQ_filepath(),qa.getQ_date()};//没有qa.getQ_date() 
   		  try {  
   	            qr.update("insert into q_a values(?,?,?,?,?,?,?,?,?,null,null,null,null,null,'0')", param);  
   	        } catch (SQLException e) {  
   	            throw new RuntimeException(e);  
   	        }  
        }
        else
        {       	       	
        	qa.setA_date(df.format(new Date()));// new Date()为获取当前系统时间
        	
        	
       	  Object[] param = { 
       			  //这里前端在提交表时需要有一个qa.getId()     
             		qa.getId_responser(),qa.getReply_content(),qa.getA_filename(),qa.getA_filepath(),qa.getA_date(),qa.getId()};
       	  try {  
                 qr.update("update q_a set id_responser=?,reply_content=?,a_filename=?,a_filepath=?,a_date=?,status='1' where id=?", param);  
             } catch (SQLException e) {  
                 throw new RuntimeException(e);  
             }  
        }     
      
   }


	@Override
	public List<QA> searchHot() throws SQLException {
		QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
		String sql = "select * from q_a where hot='1'";
		return qr.query(sql, new BeanListHandler<>(QA.class));
	}  
	
	

}
