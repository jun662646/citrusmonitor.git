package main.cn.zhku.citrusmonitor.modules.qa.dao;

import java.sql.SQLException;
import java.util.List;


import main.cn.zhku.citrusmonitor.domain.QA;

public interface QADao {
	
	
	/*public int addQ(QA qa) throws Exception;		//上传问题
	
	public int addA(QA qa) throws Exception;		//回答问题*/
	
	public List<QA> searchAll() throws Exception;  //显示为解决的问题
	

	public String getRole(String userid) throws Exception;    //根据id查询role
	
	public void insert(QA qa,String role);//上传文件  


	public List<QA> searchHot() throws SQLException;        //查找热门话题
}
