package main.cn.zhku.citrusmonitor.modules.qa.service;

import java.sql.SQLException;
import java.util.List;

import file.daoFactory.FileDaoFactory;
import main.cn.zhku.citrusmonitor.domain.QA;
import main.cn.zhku.citrusmonitor.modules.qa.dao.QADao;
import main.cn.zhku.citrusmonitor.modules.qa.dao.Impl.QADaoImpl;

/**
 * @author Ade
 *
 *@date 2018年6月18日 下午10:27:04 
 */
public class QAService {
	
	
	private QADao qaDao =new QADaoImpl();

	/*public int addQ(QA qa) throws Exception { return qaDao.addQ(qa); }

	public int addA(QA qa) throws Exception {return qaDao.addA(qa);}*/

    //FileDaoImpl实现类实例  
    QADao f= FileDaoFactory.getInstance().createDao(QADao.class);  
      
    //上传文件  
    public void insert(QA qa, String role) {  
        f.insert(qa,role);//调用dao层进行添加  
    }

	public String getRole(String userid) throws Exception {return qaDao.getRole(userid);}

	public List<QA> searchAll() throws Exception {return qaDao.searchAll();}
	
	public List<QA> searchHot() throws Exception {return qaDao.searchHot();}  

}
