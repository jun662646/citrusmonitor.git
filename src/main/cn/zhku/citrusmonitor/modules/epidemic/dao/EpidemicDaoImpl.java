package main.cn.zhku.citrusmonitor.modules.epidemic.dao;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import main.cn.zhku.citrusmonitor.util.DataSourceUtils;
import main.cn.zhku.citrusmonitor.util.StringUtil;
import main.cn.zhku.citrusmonitor.domain.Epidemic;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.junit.Test;
public class EpidemicDaoImpl implements EpidemicDao{

	@Override
	public int getCount() throws SQLException {
		QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
		String sql ="select count(*) from epidemic;";
		return ((Long)qr.query(sql, new ScalarHandler())).intValue();
	}

	@Override
	public Epidemic getEpidemicById(String id) throws SQLException {
		QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
		String sql ="select * from epidemic where id=?;";
		return qr.query(sql, new BeanHandler<Epidemic>(Epidemic.class),id);
	}

	@Override
	public int addEpidemic(Epidemic epidemic) throws SQLException {
		QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
		String sql = "insert into epidemic (id,date,temperature,adult_sum,larva_sum,egg_sum,"
				+ "detail,img,id_publisher,remark) values(?,?,?,?,?,?,?,?,?,?)";
		Object[] params = {epidemic.getId(),epidemic.getDate(),
				epidemic.getTemperature(),epidemic.getAdult_sum(),
				epidemic.getLarva_sum(),epidemic.getEgg_sum(),
				epidemic.getDetail(),epidemic.getImg(),epidemic.getId_publisher(),
				epidemic.getRemark()		
		};
		return qr.update(sql, params);
	}

	@Override
	public int modifyEpidemic(Epidemic epidemic) throws SQLException {
		QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
		String sql = "update  epidemic set date=?,temperature=?,adult_sum=?,larva_sum=?,egg_sum=?,"
				+ "detail=?,img=?,id_publisher=?,remark=? where id=?";
		Object[] param = {epidemic.getDate(),
				epidemic.getTemperature(),epidemic.getAdult_sum(),
				epidemic.getLarva_sum(),epidemic.getEgg_sum(),
				epidemic.getDetail(),epidemic.getImg(),epidemic.getId_publisher(),
				epidemic.getRemark(),epidemic.getId()		
		};
		return qr.update(sql, param);
	}

	@Override
	public int deleteEpidemicByBean(String id) throws SQLException {
		QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
		String sql = "delete from epidemic where id =?";
		return qr.update(sql, id);
	}

	@Override
	public List<Epidemic> getEpidemicList(int currentPage, int pageSize, Epidemic epidemic) throws SQLException {
		QueryRunner qr = new QueryRunner(DataSourceUtils.getDataSource());
		String sql = "select * from epidemic where 1=1";
		ArrayList<Object> params=new ArrayList<>();//用于储存参数
		//判断参数是否为空
		if (StringUtil.isNotEmpty(epidemic.getId())) {
			sql+=(" and id=? ");
			params.add(epidemic.getId());
		}
		if (StringUtil.isNotEmpty(epidemic.getDetail())) {
			sql+=(" and detail like ? ");
			params.add("%"+epidemic.getDetail()+"%");
		}
		if (StringUtil.isNotEmpty(epidemic.getRemark())) {
			sql+=("and remark=? ");
			params.add("%"+epidemic.getRemark()+"%");
		}
		if (StringUtil.isNotEmpty(epidemic.getId_publisher())) {
			sql+=(" and id_publisher=? ");
			params.add(epidemic.getId_publisher());
		}
		sql+=" limit "+(currentPage-1)*pageSize+","+pageSize+";";
		
		return qr.query(sql, new BeanListHandler<>(Epidemic.class), params.toArray());
	}
@Test
 public void fun() throws SQLException{
	Epidemic epidemic =new Epidemic();
//	epidemic.setId("22");
//	epidemic.setRemark("eclipseTestDao");
//	epidemic.setId_publisher("1");
//	epidemic.setDate(null);
//	epidemic.setAdult_sum(1);
	epidemic.setDetail("细");
//	epidemic.setEgg_sum(1);
//	epidemic.setImg("c:/");
//	epidemic.setLarva_sum(1);
//	epidemic.setTemperature((float) 26.5);
	System.out.println("总数为:"+getEpidemicList(1, 10, epidemic).toString());
// 问题：
//
//	代码中查询MySQL的结果集时报错，提示Value '0000-00-00 00:00:00' can not be represented as
//	java.sql.Timestamp;刚开始以为是代码中格式化结果集中的日期报错，找了一遍发现并没有对日期进行格式化，发现是查询的
//  结果集中某数据行的日期值为0000-00-00 00:00:00。因MySQL的时间类型datetime范围是1000-01-01 00:00:00
//  到 9999-12-31 23:59:59，所以报错。
//
//	 
//
//	解决方法：
//
//	　　将日期改为正常日期即可。
}
}
