package main.cn.zhku.citrusmonitor.modules.epidemic.dao;

import java.sql.SQLException;
import java.util.List;

import main.cn.zhku.citrusmonitor.domain.Epidemic;

public interface EpidemicDao {

	/**
	 * 获取总数据记录数
	 * @return int 记录数
	 * @throws SQLException 
	 */
	int getCount() throws SQLException;
	
	/**
	 * 根据id查询epidemic
	 * @param epidemic
	 * @return Epidemic对象
	 * @throws SQLException
	 */
	Epidemic getEpidemicById(String id) throws SQLException;
	/**
	 * 增加epidemic表
	 * @param epidemic
	 * @return 影响行数
	 * @throws SQLException
	 */
	int addEpidemic(Epidemic epidemic) throws SQLException;
	/**
	 * 修改epidemic表数据
	 * @param epidemic
	 * @return 影响行数
	 * @throws SQLException
	 */
	int modifyEpidemic(Epidemic epidemic) throws SQLException;
	/**
	 * 根据epidemic对象的id或某个条件删除
	 * @param epidemic
	 * @return 影响行数
	 * @throws SQLException
	 */
	int deleteEpidemicByBean(String id) throws SQLException; 
	/**
	 * 分页多条件查询
	 * @param currentPage
	 * @param pageSize
	 * @param epidemic
	 * @return
	 * @throws SQLException
	 */
	List<Epidemic> getEpidemicList(int currentPage, int pageSize, Epidemic epidemic) throws SQLException;
}
