package main.cn.zhku.citrusmonitor.modules.epidemic.service;

import java.sql.SQLException;
import java.util.List;

import main.cn.zhku.citrusmonitor.domain.Epidemic;
import main.cn.zhku.citrusmonitor.modules.epidemic.dao.EpidemicDao;
import main.cn.zhku.citrusmonitor.modules.epidemic.dao.EpidemicDaoImpl;

public class EpidemicService {


    private EpidemicDao dao =new EpidemicDaoImpl();
	public int add(Epidemic epidemic) throws SQLException {
		return dao.addEpidemic(epidemic);
	}
	public int delete(Epidemic epidemic) throws SQLException {
		String id = epidemic.getId();
		return dao.deleteEpidemicByBean(id);
	}
	public int edit(Epidemic epidemic) throws SQLException {
		
		return dao.modifyEpidemic(epidemic);
	}
	public int getCount() throws SQLException {
		return dao.getCount();
	}
	public List<Epidemic> list(int currentPage, int pageSize, Epidemic epidemic) throws SQLException {
		// TODO Auto-generated method stub
		return dao.getEpidemicList(currentPage, pageSize, epidemic);
	}
	public Epidemic findById(String id) throws SQLException {
		
		return dao.getEpidemicById(id);
	}
   
}
