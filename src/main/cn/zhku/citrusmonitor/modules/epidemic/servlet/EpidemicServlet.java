package main.cn.zhku.citrusmonitor.modules.epidemic.servlet;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.fileupload.FileItem;

import org.apache.commons.fileupload.ProgressListener;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import main.cn.zhku.citrusmonitor.domain.Epidemic;
import main.cn.zhku.citrusmonitor.modules.epidemic.service.EpidemicService;
import main.cn.zhku.citrusmonitor.util.UUIDUtils;
import main.cn.zhku.citrusmonitor.util.model.Message;
import main.cn.zhku.citrusmonitor.util.model.PageBean;
import main.cn.zhku.citrusmonitor.web.BaseServlet;
import net.sf.json.JSONObject;
public class EpidemicServlet extends BaseServlet {
	EpidemicService epidemicService =new EpidemicService();
	private static final long serialVersionUID = 1L;
	public JSONObject add(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		        Epidemic epidemic =new Epidemic();
		        // 得到上传文件的保存目录，将上传的文件存放于/imag/epidemic目录下
				String savePath = this.getServletContext().getRealPath("/imag/epidemic");
				// 上传时生成的临时文件保存目录
				String tempPath = this.getServletContext().getRealPath("/imag/epidemic");
				File tmpFile = new File(tempPath);
				System.out.println(savePath);
				if (!tmpFile.exists()) {
					// 创建临时目录
					tmpFile.mkdir();
				}
				
					// 使用Apache文件上传组件处理文件上传步骤：
					// 1、创建一个DiskFileItemFactory工厂
					DiskFileItemFactory factory = new DiskFileItemFactory();
					// 设置工厂的缓冲区的大小，当上传的文件大小超过缓冲区的大小时，就会生成一个临时文件存放到指定的临时目录当中。
					factory.setSizeThreshold(1024 * 100);// 设置缓冲区的大小为100KB，如果不指定，那么缓冲区的大小默认是10KB
					// 设置上传时生成的临时文件的保存目录
					factory.setRepository(tmpFile);
					// 2、创建一个文件上传解析器
					ServletFileUpload upload = new ServletFileUpload(factory);
					// 监听文件上传进度
					upload.setProgressListener(new ProgressListener() {
						public void update(long pBytesRead, long pContentLength, int arg2) {
							System.out.println("文件大小为：" + pContentLength + ",当前已处理：" + pBytesRead);
							/**
							 * 文件大小为：14608,当前已处理：4096 文件大小为：14608,当前已处理：7367
							 * 文件大小为：14608,当前已处理：11419 文件大小为：14608,当前已处理：14608
							 */
						}
					});
					// 解决上传文件名的中文乱码
					upload.setHeaderEncoding("UTF-8");
					// 3、判断提交上来的数据是否是上传表单的数据
					if (!ServletFileUpload.isMultipartContent(request)) {
						// 按照传统方式获取数据
						return null;
					}
					// 设置上传单个文件的大小的最大值，目前是设置为1024*1024字节，也就是1MB
					upload.setFileSizeMax(1024 * 1024 * 10);
					// 设置上传文件总量的最大值，最大值=同时上传的多个文件的大小的最大值的和，目前设置为10MB
					upload.setSizeMax(1024 * 1024 * 100);
					// 4、使用ServletFileUpload解析器解析上传数据，解析结果返回的是一个List<FileItem>集合，每一个FileItem对应一个Form表单的输入项
					List<FileItem> list = upload.parseRequest(request);

					/*
					 * * 把fileItemList中的数据封装到Download对象中 > 把所有的普通表单字段数据先封装到Map中 >
					 * 再把map中的数据封装到Download对象中
					 */
					Map<String, String> map = new HashMap<String, String>();
					String downloadsorce = "";
					for (FileItem item : list) {
						// 如果fileitem中封装的是普通输入项的数据
						if (item.isFormField()) {
							// 为map赋值
							map.put(item.getFieldName(), item.getString("UTF-8"));
						} else {// 如果fileitem中封装的是上传文件
							// 得到上传的文件名称，
							String filename = item.getName();
							System.out.println(filename);
							if (filename == null || filename.trim().equals("")) {
								continue;
							}
							// 注意：不同的浏览器提交的文件名是不一样的，有些浏览器提交上来的文件名是带有路径的，如：
							// c:\a\b\1.txt，而有些只是单纯的文件名，如：1.txt
							// 处理获取到的上传文件的文件名的路径部分，只保留文件名部分
							filename = filename.substring(filename.lastIndexOf("\\") + 1);
							// 得到上传文件的扩展名
							String fileExtName = filename.substring(filename.lastIndexOf(".") + 1);
							// 如果需要限制上传的文件类型，那么可以通过文件的扩展名来判断上传的文件类型是否合法
							System.out.println("上传的文件的扩展名是：" + fileExtName);
							String saveFilename = makeFileName(filename);
							File file = new File(savePath + "\\" + saveFilename);
							item.write(file);
							// 得到文件保存的名称
							downloadsorce = saveFilename;
							// 删除处理文件上传时生成的临时文件
							item.delete();
						}
					}
					// 将数据写入数据库
					
					 BeanUtils.populate(epidemic, request.getParameterMap());
//					 Epidemic epidemic = CommonUtils.toBean(map, Epidemic.class);
				     epidemic.setId(UUIDUtils.getId());
				
				     epidemic.setImg(downloadsorce);
				     System.out.println(epidemic.toString());
				     if (epidemicService.add(epidemic) == 1)
					 return JSONObject.fromObject(new Message("1", "添加成功"));
				     else 
					 return JSONObject.fromObject(new Message("2", "添加失败"));
			
				
	}
    public JSONObject delete(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Epidemic epidemic =new Epidemic();
		BeanUtils.populate(epidemic, request.getParameterMap());
		if (epidemicService.delete(epidemic) == 1)
		return JSONObject.fromObject(new Message("1", "删除成功"));
		else 
		return JSONObject.fromObject(new Message("2", "删除失败"));

	}
    public JSONObject edit(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Epidemic epidemic =new Epidemic();
		BeanUtils.populate(epidemic, request.getParameterMap());
		if (epidemicService.edit(epidemic) == 1)
			return JSONObject.fromObject(new Message("1", "修改成功"));
		else 
		return JSONObject.fromObject(new Message("2", "修改失败"));

	}
    
    /**多条件分页查询
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    public JSONObject list(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Epidemic epidemic =new Epidemic();
		
		PageBean<Epidemic> page = new PageBean<Epidemic>();
		BeanUtils.populate(epidemic, request.getParameterMap());
		BeanUtils.populate(page, request.getParameterMap());
		int currentPage = page.getPageNum(); //当前页
		int pageSize = page.getPageSize(); //每页条数
		int totalCount = 0; //总条数
		List<Epidemic> epidemicList =  epidemicService.list(currentPage, pageSize, epidemic);
		totalCount = epidemicService.getCount();
        //  返回 pageBean
        return JSONObject.fromObject(new PageBean<Epidemic>(epidemicList, currentPage, pageSize, totalCount));
	}

    private String makeFileName(String filename) {
		// 为防止文件覆盖的现象发生，要为上传文件产生一个唯一的文件名
		return UUID.randomUUID().toString() + "." + filename.substring(filename.lastIndexOf(".") + 1);
	}
}
