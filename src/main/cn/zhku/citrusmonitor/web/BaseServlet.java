package main.cn.zhku.citrusmonitor.web;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.cn.zhku.citrusmonitor.util.model.Message;
import net.sf.json.JSONObject;

/**
 * 通用servlet
 * 
 * @author 成君 943193747@qq.com
 * @Date 2018年6月16日 下午3:50:45
 */
public class BaseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			// 1.获取子类 创建子类或者调用子类的时候 this代表的是子类对象
			Class<? extends BaseServlet> clazz = this.getClass();

			// 2.获取请求的方法
			String m = request.getParameter("method");
			if (m == null) {
				m = "index";
			}

			// 3.获取方法对象
			Method method = null;
			try {
				method = clazz.getMethod(m, HttpServletRequest.class, HttpServletResponse.class);
			} catch (Exception e) {
				System.out.println(new Message(m, "方法不存在").toString());
				request.getRequestDispatcher(request.getContextPath() + "/error/404.jsp").forward(request, response);
				return;
			}

			Object obj = null;
			if (method != null) {
				// 4.让方法执行 返回值为请求转发的路径
				obj = (Object) method.invoke(this, request, response);// 相当于 userservlet.add(request,response)
				if (obj != null) {
					// 将信息写回页面
					response.getWriter().write(obj+"");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}

	}

	public String index(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		return null;
	}
}