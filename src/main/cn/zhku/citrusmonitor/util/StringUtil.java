package main.cn.zhku.citrusmonitor.util;

/**
 * @author 成君 943193747@qq.com
 * @Date 2018年6月17日  上午10:45:45
 */
public class StringUtil {
	
	/**
	 * 判断字符串是否为空
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str) {
		
		if (str==null||"".equals(str)) {
			return true;
		}else
		return false;
		
	}
	
	/**
	 * 判断字符串是否不为空
	 * @param str
	 * @return
	 */
	public static boolean isNotEmpty(String str){
		if(str!=null && str.trim().length()>0){
			return true;
		}else{
			return false;
		}
	}
}
