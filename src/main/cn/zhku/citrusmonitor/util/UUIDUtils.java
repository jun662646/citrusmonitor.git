package main.cn.zhku.citrusmonitor.util;

import java.util.UUID;

/**
 * 一个用于生成UID的工具类
 * 
 * @author 成君 943193747@qq.com
 * @Date 2018年6月15日  上午11:10:19
 */
public class UUIDUtils {
	/**
	 * 随机生成id
	 * @return
	 */
	public static String getId(){
		return UUID.randomUUID().toString().replace("-", "").toUpperCase();
	}
	
	/**
	 * 生成随机码
	 * @return
	 */
	public static String getCode(){
		return getId();
	}
	
	public static void main(String[] args) {
		System.out.println(getId());
	}
}
