package main.cn.zhku.citrusmonitor.util.model;

import java.util.List;

/**
 * @author 成君 943193747@qq.com
 * @Date 2018年6月17日 上午9:41:44
 */
@SuppressWarnings("unused")
public class PageBean<E> {
	private List<E> list;
	// 当前请求页码，默认为1
	private Integer pageNum = 1;
	// 一页条数，默认为10
	private Integer pageSize = 10;
	// 总页数
	private Integer totalPage;
	// 总条数
	private Integer totalCount;

	public PageBean() {
	}

	public PageBean(List<E> list, Integer pageNum, Integer pageSize, Integer totalCount) {
		this.list = list;
		this.pageNum = pageNum;
		this.pageSize = pageSize;
		this.totalCount = totalCount;
	}

	public List<E> getList() {
		return list;
	}

	public void setList(List<E> list) {
		this.list = list;
	}

	public Integer getPageNum() {
		return pageNum;
	}

	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getTotalPage() {
		return (int) Math.ceil(totalCount * 1.0 / pageSize);
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

}
