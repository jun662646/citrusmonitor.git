package main.cn.zhku.citrusmonitor.util.model;

/**
 * 状态信息类
 * 
 * @author 成君 943193747@qq.com
 * @Date 2018年6月16日 下午4:14:06
 */
public class Message {
	
	private String status;	// 状态码
	private String msg;		// 具体信息
	private String key;		// 用来放各种关键的属性，如userId

	public Message(String status, String msg) {
		this.status = status;
		this.msg = msg;
	}

	public Message(String status, String msg, String key) {
		this.status = status;
		this.msg = msg;
		this.key = key;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	@Override
	public String toString() {
		return "{\"status\":\"" + status + "\" ,\"msg\":\"" + msg+"\" ,\"key\":\"" + key + "\"}";
	}
}
