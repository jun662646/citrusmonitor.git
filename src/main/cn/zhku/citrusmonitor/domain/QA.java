package main.cn.zhku.citrusmonitor.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 成君 943193747@qq.com
 * @Date 2018年6月16日  下午3:02:16
 */
public class QA implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String id;
	private String name;
	private String phone;
	private String theme;
	private String q_details;
	private String reply_content;
	private String q_date;
	private String a_date;
	private String q_filepath;
	private String a_filepath;
	private String q_filename;
	private String a_filename;
	private String id_responser;
	private String remark;
	private String status;
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getQ_filepath() {
		return q_filepath;
	}
	public void setQ_filepath(String q_filepath) {
		this.q_filepath = q_filepath;
	}
	public String getA_filepath() {
		return a_filepath;
	}
	public void setA_filepath(String a_filepath) {
		this.a_filepath = a_filepath;
	}
	public String getQ_filename() {
		return q_filename;
	}
	public void setQ_filename(String q_filename) {
		this.q_filename = q_filename;
	}
	public String getA_filename() {
		return a_filename;
	}
	public void setA_filename(String a_filename) {
		this.a_filename = a_filename;
	}
		
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone == null ? null : phone.trim();
	}
	public String getTheme() {
		return theme;
	}
	public void setTheme(String theme) {
		this.theme = theme == null ? null : theme.trim();
	}
	public String getQ_details() {
		return q_details;
	}
	public void setQ_details(String q_details) {
		this.q_details = q_details;
	}
	public String getReply_content() {
		return reply_content;
	}
	public void setReply_content(String reply_content) {
		this.reply_content = reply_content;
	}
	public String getQ_date() {
		return q_date;
	}
	public void setQ_date(String q_date) {
		this.q_date = q_date;
	}
	public String getA_date() {
		return a_date;
	}
	public void setA_date(String string) {
		this.a_date = string;
	}
	public String getId_responser() {
		return id_responser;
	}
	public void setId_responser(String id_responser) {
		this.id_responser = id_responser == null ? null : id_responser.trim();
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	@Override
	public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", phone=").append(phone);
        sb.append(", theme=").append(theme);
        sb.append(", q_details=").append(q_details);
        sb.append(", reply_content=").append(reply_content);
        sb.append(", q_date=").append(q_date);
        sb.append(", a_date=").append(a_date);        
        sb.append(", q_filepath=").append(q_filepath);
        sb.append(", a_filepath=").append(a_filepath);
        sb.append(", q_filename=").append(q_filename);
        sb.append(", a_filename=").append(a_filename);
        sb.append(", id_responser=").append(id_responser);
        sb.append(", remark=").append(remark);
        sb.append(", status=").append(status);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
	}
	
}
