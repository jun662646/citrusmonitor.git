package main.cn.zhku.citrusmonitor.domain;

import java.io.Serializable;
import java.util.List;

public class FerJson implements Serializable {
	private String left_list;
	private String right_title;
	private String right_list;
	public String getLeft_list() {
		return left_list;
	}
	public void setLeft_list(String left_list) {
		this.left_list = left_list;
	}
	public String getRight_title() {
		return right_title;
	}
	public void setRight_title(String right_title) {
		this.right_title = right_title;
	}
	public String getRight_list() {
		return right_list;
	}
	public void setRight_list(String right_list) {
		this.right_list = right_list;
	}
}
