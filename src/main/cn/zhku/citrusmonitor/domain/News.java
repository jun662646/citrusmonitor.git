package main.cn.zhku.citrusmonitor.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 成君 943193747@qq.com
 * @Date 2018年6月16日  下午3:11:32
 */
public class News implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String id;
	private String id_type;        
	private String topic;
	private String content;
	private String img_src;
	private Date date;
	private String id_publisher;
	private String remark;
	private String title;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}
	public String getId_type() {
		return id_type;
	}
	public void setId_type(String id_type) {
		this.id_type = id_type;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getId_publisher() {
		return id_publisher;
	}
	public void setId_publisher(String id_publisher) {
		this.id_publisher = id_publisher == null ? null : id_publisher.trim();
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	@Override
	public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", id_type=").append(id_type);
        sb.append(", topic=").append(topic);
        sb.append(", content=").append(content);
        sb.append(", picture=").append(img_src);
        sb.append(", date=").append(date);
        sb.append(", id_publisher=").append(id_publisher);
        sb.append(", remark=").append(remark);
        sb.append(", title=").append(title);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
	}
	public String getImg_src() {
		return img_src;
	}
	public void setImg_src(String img_src) {
		this.img_src = img_src;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

}
