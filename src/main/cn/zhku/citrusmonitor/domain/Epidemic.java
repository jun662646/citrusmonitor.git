package main.cn.zhku.citrusmonitor.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 成君 943193747@qq.com
 * @Date 2018年6月16日  下午3:27:43
 */
public class Epidemic implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String id;
	private Date date;
	private Float temperature;
	private Integer adult_sum;
	private Integer larva_sum;
	private Integer egg_sum;
	private String detail;
	private String id_publisher;
	private String img;
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	private String remark;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Float getTemperature() {
		return temperature;
	}
	public void setTemperature(Float temperature) {
		this.temperature = temperature;
	}
	public Integer getAdult_sum() {
		return adult_sum;
	}
	public void setAdult_sum(Integer adult_sum) {
		this.adult_sum = adult_sum;
	}
	public Integer getLarva_sum() {
		return larva_sum;
	}
	public void setLarva_sum(Integer larva_sum) {
		this.larva_sum = larva_sum;
	}
	public Integer getEgg_sum() {
		return egg_sum;
	}
	public void setEgg_sum(Integer egg_sum) {
		this.egg_sum = egg_sum;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getId_publisher() {
		return id_publisher;
	}
	public void setId_publisher(String id_publisher) {
		this.id_publisher = id_publisher;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	@Override
	public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", date=").append(date);
        sb.append(", temperature=").append(temperature);
        sb.append(", adult_sum=").append(adult_sum);
        sb.append(", larva_sum=").append(larva_sum);
        sb.append(", egg_sum=").append(egg_sum);
        sb.append(", detail=").append(detail);
        sb.append(", id_publisher=").append(id_publisher);
        sb.append(", remark=").append(remark);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
	}
	
}
