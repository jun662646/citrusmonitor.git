package main.cn.zhku.citrusmonitor.domain;

import java.io.Serializable;

/**
 * @author 成君 943193747@qq.com
 * @Date 2018年6月16日  下午2:51:59
 */

public class Dictionary implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String id;
    private String name;
    private String pid;
    private String remark;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }
    public String getPid() {
        return pid;
    }
    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }
    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", pid=").append(pid);
        sb.append(", remark=").append(remark);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}